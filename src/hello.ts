import prompt from "promise-prompt";
import colors from "colors";

/**
 * A function asking user for a name. Print a message with that name.
 *
 */
export async function hello() {
  const name = prompt("What is your name? ");
  console.log(creatHelloText(await name));
}

/**
 * A function taking a string and return a string with ascii art and  escape charaters.
 *
 * @param msg A message to be modified.
 * @returns A new text based on 'msg'.
 */
export function creatHelloText(msg: string) {
  return "Hello " + colors.bold.red(msg) + "!";
}
