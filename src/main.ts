#!/usr/bin/env node
import { hello } from "./hello.js";

async function main() {
  hello();
}

main();
